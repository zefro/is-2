<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crime extends Model
{
    protected $fillable = [
        'ob_number','location' ,'police_station_name',
        'investigating_officer_id',
        'ocs_id','report', 'evidence', 'image_name','image_url','status',
        'user_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];
}
