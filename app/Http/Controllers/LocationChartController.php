<?php

namespace App\Http\Controllers;
use App\Crime;
use App\County;
use DB;
use App\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LocationChartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateLocationChart()
    {
        
        $groups = DB::table('crimes')
                  ->select('location', DB::raw('count(*) as total'))
                //   ->where('status',$reported_crimes)
                  ->groupBy('location')
                  ->pluck('total', 'location')->all();
                  
// Generate random colours for the groups
// for ($i=0; $i<=count($groups); $i++) {
//             $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
//         }

        $colours = array(
            '#FEA47F',
            '#ef5777',
            '#4cd137',
            '#2C3A47',
            '#82589F',
            '#00a8ff',
            '#9c88ff',
            '#fbc531',
            '#4cd137',
            '#487eb0',
            '#e84118',
            '#f5f6fa',
            '#7f8fa6',
            '#273c75',
            '#353b48',
            '#ef5777',
            '#575fcf',
            '#4bcffa',
            '#34e7e4',
            '#0be881',
            '#ffc048'
        );
        $key = array_rand($colours);
        
// Prepare the data for returning with the view
$chart = new Chart;
        $chart->labels = (array_keys($groups));
        $chart->dataset = (array_values($groups));
        $chart->colours = $colours;

        $locations = County::all(); 
        return view('charts.location', compact('chart','locations'));
    }
    
}
