<?php

namespace App\Http\Controllers\InvestigatingOfficer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\InvestigatingOfficer;

class InvestigatingOfficerLoginController extends Controller
{
 
    public function LoginForm(){
        return view('investigatingofficers.login');
    }
    
    public function Authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        if (Auth::guard('investigatingofficer')->attempt(['email'=>$request->email, 'password'=>$request->password , 'status'=>1])){
            return redirect(route('io.home'));
        }

        return redirect(route('io.login.form'))->with('error', 'Opps! You have entered invalid credentials');
    
    }
  

}
