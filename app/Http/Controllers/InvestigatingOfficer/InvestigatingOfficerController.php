<?php

namespace App\Http\Controllers\InvestigatingOfficer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Crime;
use App\CaseFile;
class InvestigatingOfficerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:investigatingofficer');
    }
    public function index(){
        $io_id = Auth::user()->io_pin;

        //count assigned crimes
        $io_id_crimes = Crime::all()->where(['investigating_officer_id'],$io_id);
        $count_assigned=count($io_id_crimes);

        //count submitted crimes
        $io_id_crimes_submitted = Crime::all()->where(['investigating_officer_id'],$io_id);
        $io_id_crimes_submitted=$io_id_crimes_submitted->where(['status'],'recomendation from io');
        $count_submitted=count($io_id_crimes_submitted);
        return view('investigatingofficers.index')->with('count_assigned',$count_assigned);
    }

    // public function countSubmitted(){
    //     $io_id = Auth::user()->io_pin;
    //     //count submitted crimes
    //     $io_id_crimes_submitted = Crime::all()->where(['investigating_officer_id'],$io_id);
    //     $io_id_crimes_submitted=$io_id_crimes_submitted->where(['status'],'2');
    //     $count_submitted=count($io_id_crimes_submitted);
    //     return view('investigatingofficers.index')->with('count_submitted', $count_submitted);
    // }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/investigatingofficer/login');
    }
    public function viewCrimes(){
        $code = Auth::user()->io_pin;
        $crimes = Crime::all()->where(['investigating_officer_id'],$code);
        return view('investigatingofficers.assigned')->with('crimes',$crimes,$code); 
    }
    public function createCase($id){
        $casefile = CaseFile::find($id);
        return view('investigatingofficers.new_case',compact('id','casefile'));
    }
    // public function updateCrime($id){
    //     $crime = Crime::find($id);
    //     $crime->status ="3" ;
    //    }
    public function submitCase(Request $request ,$id){
        $crime = Crime::find($id);
        $crime->status ="recomendation from io" ;
        $crime->save();
        
        $io_id = 
        Auth::user()->io_pin;
        $casefile = CaseFile::find($id);
        $casefile->witness_statement = $request->get('witness_statement');
        $casefile->report = $request->get('report');
        $casefile->suspect_1_id = $request->get('suspect_1_id');
        $casefile->suspect_1_first_name = $request->get('suspect_1_first_name');
        $casefile->suspect_1_last_name = $request->get('suspect_1_last_name');
        $casefile->suspect_1_sex = $request->get('suspect_1_sex');
        $casefile->recomendation = $request->get('recomendation');
        $casefile->case_status="recomendation from io";
        $casefile->io_id = $io_id;
        $casefile->save();
        return redirect('investigatingofficer/home')->with('success','Data Updated');
    }
    //case status 1 -> a new case thats not assigned to an investigating officer
    //case status 2-> a case that is being worked on by an investigating officer
    //case status 3-> a case that is being worked on by a prosecutor
    //case status 69-> closed by recomendation of an investigating officer by the ocs
}
