<?php

namespace App\Http\Controllers\Judicialofficer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\CaseFile;
use App\Offender;
use App\Crime;
class JudicialOfficerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:judicialofficer');
    }
    public function index(){
        return view('judicialofficers.index');
    }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/judicialofficer/login');
    }
    public function viewCases(){
        $jo_id = Auth::user()->jo_id;
        $cases = CaseFile::all()->where(['jo_id'],$jo_id);
        return view('judicialofficers.cases')->with('cases',$cases,$jo_id); 
    }
    public function viewCase($id){
        $case = CaseFile::find($id);  
        return view('judicialofficers.view_case')->with('case', $case, $id);
    }
    public function updateCase(Request $request, $id){  
        $crime = Crime::find($id);
        $crime->status ="closed";
        $crime->save();

        $case_file = CaseFile::find($id);
        $case_file->case_status = "closed";
        $case_file->save();
        return redirect('judicialofficer/home');
    }
    public function offenderForm($id){
        $case = CaseFile::find($id);  
        return view('judicialofficers.add_offender')->with('case',$case);
    }
    public function addOffender(Request $request ,$id){

        function generatePin($length = 8)
        {
            $chars = '0123456789';
            $count = mb_strlen($chars);
    
            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }
            return $result;
        }
        $case = CaseFile::find($id);  
        $ob_number=CaseFile::find($id)->case_id;

        $offender_id= generatePin();

        $offender = New Offender();
        $offender->ob_number = $ob_number;
        $offender->national_id = $request->national_id;
        $offender->first_name = $request->first_name;
        $offender->last_name = $request->last_name;
        $offender->sex = $request->sex;
        $offender->offender_id = $offender_id;
        $offender->plea = $request->plea;
        $offender->offence = $request->offence;
        $offender->sentence = $request->sentence;
        $offender->bail = $request->bail;
        $offender->save();
        
        return redirect('judicialofficer/view/cases/')->with('Success', 'Data Added');
    }
    // public function updateOffender(Request $request, $id){
    //     $offender_id = $this->generatePin();

    //     $case = CaseFile::find($id);  
        // $offender = new Offender;
        // $offender->national_id = $request->national_id;
        // $offender->first_name = $request->first_name;
        // $offender->last_name = $request->last_name;
        // $offender->sex = $request->sex;
        // $offender->offender_id = $offender_id;
        // $offender->plea = $request->plea;
        // $offender->offence = $request->offence;
        // $offender->sentence = $request->sentence;
        // $offender->bail = $request->bail;
        // $offender->save();
        // dd($id);
    //     return view('judicialofficers.add_offender')->with('case','offender',$offender, $case);
    // }
    
}
