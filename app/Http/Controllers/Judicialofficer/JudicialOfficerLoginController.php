<?php

namespace App\Http\Controllers\Judicialofficer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\JudicialOfficer;

class JudicialOfficerLoginController extends Controller
{
 
    public function LoginForm(){
        return view('judicialofficers.login');
    }
    
    public function Authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        if (Auth::guard('judicialofficer')->attempt(['email'=>$request->email, 'password'=>$request->password , 'status'=>1])){
            return redirect(route('jo.home'));
        }

        return redirect(route('jo.login.form'))->with('error', 'Opps! You have entered invalid credentials');
    
    }
  

}
