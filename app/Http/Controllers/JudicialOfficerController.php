<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\JudicialOfficer;
use Auth;
use DB;

class JudicialOfficerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $judicial_officers = DB::SELECT('SELECT * FROM judicial_officers');
        return view('judicialofficer.index', ['judicial_officers'=> $judicial_officers]);
    }
    public function create(){
        
        return view('judicialofficer.new');
    }
     //saves the details of the judge to the database
     public function store(Request $request)
     {
         $this->validate($request,[
         
             'email'=>'required|unique:judicial_officers',
             'first_name'=>'required',
             'last_name'=>'required',
            //  'phone_number' => 'required|min:10|max:13|size:10|unique:judicial_officers',
            //  'type'=>'required',           
 
         ]);
 
         $pin = $this->generatePin();
         $jo_id = $this->generatePin();
 
         //extends the user class
        
            $judicialofficer = new JudicialOfficer();
            $judicialofficer->password = Hash::make($pin);
            $judicialofficer->jo_id = $jo_id;
            $judicialofficer->email = $request->email;
            $judicialofficer->first_name = $request->first_name;
            $judicialofficer->last_name = $request->last_name;
            $judicialofficer->status = 1;
            $judicialofficer->save();
            $detail = "You have been added as a Judicial Officer";
        $email = $request->email;
        $year = date("Y");
        $sender = "ECourtroom";
    
        Mail::send('judicialofficers.emails.created', ['name' => $request->first_name, 'password' => $pin, 'detail' => $detail, 'year' => $year, 'sender' => $sender],
            function ($message) use ($email) {
                $message->from('info@ecourtroom.org', 'ECourtroom');
                $message->to($email)->subject('Judicial Officer Added');
            });
    
         return redirect('/judicialofficer');
        }

        public function deactivate($id){
            $judicial_officer=JudicialOfficer::find($id);
            $judicial_officer->status= 2;
            $judicial_officer->save();
            return redirect('judicialofficer')->with('success', ' Deactivated successfully');
        }
        public function activate($id){
            $judicial_officer = JudicialOfficer::find($id);
            $judicial_officer ->status = 1;
            $judicial_officer->save();
            return redirect('judicialofficer')->with('success', ' Activated successfully');

        }
        public function edit($id){
        
            $judicial_officer = JudicialOfficer::find($id);
            return view('judicialofficer.edit', compact('judicial_officer','id'));
        }
    
        public function update(Request $request , $id){
            $this->validate($request , [
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
            $judicial_officer = JudicialOfficer::find($id);
            $judicial_officer->first_name = $request->get('first_name');
            $judicial_officer->last_name = $request->get('last_name');
            $judicial_officer->save();
            return redirect('judicialofficer')->with('success','Data Updated');
        }
        function generatePin($length = 4)
        {
            $chars = '0123456789';
            $count = mb_strlen($chars);
    
            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }
            return $result;
        }
}
