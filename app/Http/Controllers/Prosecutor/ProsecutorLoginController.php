<?php

namespace App\Http\Controllers\Prosecutor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Prosecutor;

class ProsecutorLoginController extends Controller
{
    public function LoginForm(){
        return view('prosecutors.login');
    }
    
    public function Authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        if (Auth::guard('prosecutor')->attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=> 1])){
            return redirect(route('prosecutor.home'));
        }

        return redirect(route('prosecutor.login.form'))->with('error', 'Opps! You have entered invalid credentials');
    
    }
}
