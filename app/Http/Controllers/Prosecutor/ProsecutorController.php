<?php

namespace App\Http\Controllers\Prosecutor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\JudicialOfficer;
use App\CaseFile;
use App\Crime;

class ProsecutorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:prosecutor');
    }
    
    public function index(){
        return view('prosecutors.index');
    }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/prosecutor/login');
    }
    public function viewCases(){
        $prosecutor_id = Auth::user()->prosecutor_id;
        $cases = CaseFile::all()->where(['prosecutor_id'],$prosecutor_id);;
        return view('prosecutors.view_cases')->with('cases', $cases, $prosecutor_id);
    }
    public function viewCase($id){
        $case = CaseFile::find($id);  
        return view('prosecutors.view_case')->with('case', $case, $id);
    }
    public function assignJo($id){
        $jos = JudicialOfficer::all();
            $case_file = CaseFile::find($id); 
            return view('prosecutors.assign', compact('case_file','id', 'jos'));
    }
    public function updateJo(Request $request, $id){
        $crime = Crime::find($id);
        $crime->status ="in hearing";
        $crime->save();
        
        $casefile = CaseFile::find($id);
        $casefile->jo_id=$request->jo_id;
        $casefile->case_status="in hearing";
        $casefile->save();
        return redirect('/prosecutor/home')->with('success','Data Updated');
    }
}
