<?php

namespace App\Http\Controllers;
use App\Crime;
use Illuminate\Http\Request;
use Auth;

class CrimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function ShowCrime(){
        $crimes = Crime::all(); 
        return view('crime.index')->with('crimes',$crimes);
    }
}
