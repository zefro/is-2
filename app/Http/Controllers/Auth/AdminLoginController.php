<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
class AdminLoginController extends Controller
{
public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function LoginForm(){
        return view('admin.login');
    }
    public function Authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

    

        if (Auth::guard('admin')->attempt(['email'=>$request->email, 'password'=>$request->password])){
            return redirect(route('admin.home'));
        }

        return redirect(route('admin.login'))->with('error', 'Opps! You have entered invalid credentials');
    }

}
