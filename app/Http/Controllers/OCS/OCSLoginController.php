<?php

namespace App\Http\Controllers\OCS;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\OCS;

class OCSLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:ocs');
    }
 
    public function LoginForm(){
        return view('ocss.login');
    }
    
    public function Authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        if (Auth::guard('ocs')->attempt(['email'=>$request->email, 'password'=>$request->password , 'status'=>1])){
            return redirect(route('ocss.home'));
        }

        return redirect(route('ocs.login.form'))->with('error', 'Opps! You have entered invalid credentials');
    
    }
  

}
