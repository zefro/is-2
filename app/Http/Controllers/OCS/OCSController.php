<?php

namespace App\Http\Controllers\OCS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Crime;
use App\InvestigatingOfficer;
use App\CaseFile;
use App\Prosecutor;
class OCSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:ocs');
    }
    public function index(){
        $police_station_code = Auth::user()->police_station_code;
        $crimes = Crime::all()->where(['police_station_name'],$police_station_code);
        $count_crimes=count($crimes);
       
        return view('ocss.index')->with('count_crimes',$count_crimes);
    }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/ocs/login');
    }
    public function viewCrimes(){
        $code = Auth::user()->police_station_code;
        $crimes = Crime::all()->where(['police_station_name'],$code);
        return view('ocss.crimes')->with('crimes',$crimes,$code); 

    }
    public function assignCrime($id){
        $investigatingofficers = InvestigatingOfficer::all();
            $crime = Crime::find($id);  
            return view('ocss.assign', compact('crime','id', 'investigatingofficers'));
    }
    public function updateCrime(Request $request , $id){
        $ocs_id = Auth::user()->ocs_id;
        $crime = Crime::find($id);
        $crime->status = "assigned to io";
        $crime->investigating_officer_id = $request->io_id;
        $crime->ocs_id=$ocs_id;
        $crime->save();
        $ob=Crime::find($id)->ob_number;
        

        $casefile = New CaseFile();
        $casefile->case_id=$ob;
        $casefile->io_id="0";
        $casefile->jo_id="0";
        $casefile->ocs_id=$ocs_id;
        $casefile->prosecutor_id="0";
        $casefile->witness_statement="0";
        $casefile->report="0";
        $casefile->suspect_1_id="0";
        $casefile->suspect_1_first_name="0";
        $casefile->suspect_1_last_name="0";
        $casefile->suspect_1_sex="0";
        $casefile->case_status="assigned to io";
        $casefile->recomendation="0";
        $casefile->save();
        return redirect('ocs/home')->with('success','Data Updated');
    }
    public function viewCase($id){
        $case = CaseFile::find($id);
        return view('ocss.cases',compact('case'));
    }
    public function assignProsecutor($id){
        $casefile = CaseFile::find($id);
        $prosecutors = Prosecutor::all();
        return view('ocss.assign_prosecutor',compact('prosecutors' ,'id'));

    }
    public function updateProsecutor(Request $request , $id){
        $prosecutors = Prosecutor::all();

        $crime = Crime::find($id);
        $crime->status = "assigned to prosecutor";
        $crime->save();

        $casefile = CaseFile::find($id);
        $casefile->prosecutor_id = $request->prosecutor_id;
        $casefile->case_status="assigned to prosecutor";
        $casefile->recomendation ="0";
        $casefile->save();
        return redirect('ocs/home');
    }
    public function closeFile($id){
        $crime = Crime::find($id);
        $crime->status = "closed";
        $crime->save();
        
        $casefile = CaseFile::find($id);
        $casefile->case_status="closed";
        $casefile->recomendation ="0";
        $casefile->save();
        return redirect('/ocs/home');

    }

    public function showIO(){
        $investigating_officers = InvestigatingOfficer::all();
        return view('ocss.show_io')->with('investigating_officers',$investigating_officers);
    }
}
