<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\InvestigatingOfficer;
use Auth;
use App\PoliceStation;
use DB;

class InvestigatingOfficerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $investigating_officers = DB::SELECT('SELECT * FROM investigating_officers');
        return view('investigatingofficer.index', ['investigating_officers'=> $investigating_officers]);
    }
    public function create(){
        
        return view('investigatingofficer.new');
    }
     //saves the details of the judge to the database
     public function store(Request $request)
     {
         $this->validate($request,[
         
             'email'=>'required|unique:investigating_officers',
             'first_name'=>'required',
             'last_name'=>'required',
            //  'phone_number' => 'required|min:10|max:13|size:10|unique:judicial_officers',
            //  'type'=>'required',           
 
         ]);
 
         $pin = $this->generatePin();
         $io_id= $this->generatePin();
 
         //extends the user class
        
            $investigatingofficer = new InvestigatingOfficer();
            $investigatingofficer->password = Hash::make($pin);
            $investigatingofficer->io_pin = $io_id;
            $investigatingofficer->email = $request->email;
            $investigatingofficer->first_name = $request->first_name;
            $investigatingofficer->last_name = $request->last_name;
            $investigatingofficer->police_station_code = $request->police_station_code;
            $investigatingofficer->status = 1;
            $investigatingofficer->save();
            $detail = "You have been added as an Investigating Officer";
        $email = $request->email;
        $year = date("Y");
        $sender = "ECourtroom";
    
        Mail::send('investigatingofficers.emails.created', ['name' => $request->first_name, 'password' => $pin, 'detail' => $detail, 'year' => $year, 'sender' => $sender],
            function ($message) use ($email) {
                $message->from('info@ecourtroom.org', 'ECourtroom');
                $message->to($email)->subject('Investigating Officer Added');
            });
    
         return redirect('/investigatingofficer');
        }

        public function deactivate($id){
            $investigating_officer=InvestigatingOfficer::find($id);
            $investigating_officer->status= 2;
            $investigating_officer->save();
            return redirect('investigatingofficer')->with('success', ' Deactivated successfully');
        }
        public function activate($id){
            $investigating_officer = InvestigatingOfficer::find($id);
            $investigating_officer ->status = 1;
            $investigating_officer->save();
            return redirect('investigatingofficer')->with('success', ' Activated successfully');

        }
        public function edit($id){
        
            $investigating_officer = InvestigatingOfficer::find($id);
            return view('investigatingofficer.edit', compact('investigating_officer','id'));
        }
    
        public function update(Request $request , $id){
            $this->validate($request , [
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
            $investigating_officer = InvestigatingOfficer::find($id);
            $investigating_officer->first_name = $request->get('first_name');
            $investigating_officer->last_name = $request->get('last_name');
            $investigating_officer->save();
            return redirect('investigatingofficer')->with('success','Data Updated');
        }
        function generatePin($length = 4)
        {
            $chars = '0123456789';
            $count = mb_strlen($chars);
    
            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }
            return $result;
        }
        public function getData(){
            $policeposts = PoliceStation::all();                              
            return view('investigatingofficer.new', compact('policeposts'));
        }
}
