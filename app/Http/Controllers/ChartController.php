<?php

namespace App\Http\Controllers;
use App\Crime;
use DB;
use App\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ChartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateChart()
    {
        
        $groups = DB::table('crimes')
                  ->select('status', DB::raw('count(*) as total'))
                //   ->where('status',$reported_crimes)
                  ->groupBy('status')
                  ->pluck('total', 'status')->all();
                  
// Generate random colours for the groups
// for ($i=0; $i<=count($groups); $i++) {
//             $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
//         }

        $colours = array(
            '#00b894',
            '#00cec9',
            '#0984e3',
            '#ff7675',
            '#00cec9',
            '#d63031'
        );
        $key = array_rand($colours);
        
// Prepare the data for returning with the view
$chart = new Chart;
        $chart->labels = (array_keys($groups));
        $chart->dataset = (array_values($groups));
        $chart->colours = $colours;
return view('charts.index', compact('chart'));
    }


}
