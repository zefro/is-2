<?php

namespace App\Http\Controllers;
use App\CaseFile;
use Illuminate\Http\Request;
use Auth;

class CaseFileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function ShowCases(){
        $cases = CaseFile::all(); 
        
        return view('cases.index')->with('cases',$cases);
    }
}
