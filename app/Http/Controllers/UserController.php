<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $users =DB::SELECT('SELECT * FROM users');
        return view('user.index', ['users'=> $users]);
        
     }
     public function activate($id){
        $users=User::find($id);
        $users->status= 1;
        $users->save();
        return redirect('user')->with('success', ' Activated successfully');
     }
     public function deactivate($id){
        $users=User::find($id);
        $users->status= 2;
        $users->save();
        return redirect('user')->with('success', ' Dectivated successfully');
         
    }
}
