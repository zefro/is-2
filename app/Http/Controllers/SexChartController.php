<?php

namespace App\Http\Controllers;
use App\Offender;
use DB;
use App\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SexChartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateSexChart()
    {
        
        $groups = DB::table('offenders')
                  ->select('sex', DB::raw('count(*) as total'))
                //   ->where('status',$reported_crimes)
                  ->groupBy('sex')
                  ->pluck('total', 'sex')->all();
                  
// Generate random colours for the groups
// for ($i=0; $i<=count($groups); $i++) {
//             $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
//         }

        $colours = array(
            '#fd79a8',
            '#0984e3'
        );
        $key = array_rand($colours);
        
// Prepare the data for returning with the view
$chart = new Chart;
        $chart->labels = (array_keys($groups));
        $chart->dataset = (array_values($groups));
        $chart->colours = $colours;
return view('charts.sex', compact('chart'));
    }


}
