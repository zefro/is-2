<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\OCS;
use App\PoliceStation;
use Auth;
use DB;

class OCSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $ocss = DB::SELECT('SELECT * FROM officer_commanding_station');
        return view('ocs.index', ['ocss'=> $ocss]);
    }
    public function create(){
        
        return view('ocs.new');
    }
     //saves the details of the judge to the database
     public function store(Request $request)
     {
         $this->validate($request,[
         
             'email'=>'required|unique:officer_commanding_station',
             'first_name'=>'required',
             'last_name'=>'required',
            //  'phone_number' => 'required|min:10|max:13|size:10|unique:judicial_officers',
            //  'type'=>'required',           
 
         ]);
 
         $pin = $this->generatePin();
         $ocs_id = $this->generatePin();
 
         //extends the user class
        
            $ocs = new OCS();
            $ocs->password = Hash::make($pin);
            $ocs->ocs_id = $ocs_id;
            $ocs->email = $request->email;
            $ocs->first_name = $request->first_name;
            $ocs->last_name = $request->last_name;
            $ocs->police_station_code = $request->police_station_code;
            $ocs->status = 1;
            $ocs->save();
            // dd($ocs);
            $detail = "You have been added as the OCS";
        $email = $request->email;
        $year = date("Y");
        $sender = "ECourtroom";
    
        Mail::send('ocss.emails.created', ['name' => $request->first_name, 'password' => $pin, 'detail' => $detail, 'year' => $year, 'sender' => $sender],
            function ($message) use ($email) {
                $message->from('info@ecourtroom.org', 'ECourtroom');
                $message->to($email)->subject('OCS ADDED');
            });
    
         return redirect('/ocs');
        }

        public function deactivate($id){
            $ocs=OCS::find($id);
            $ocs->status= 2;
            $ocs->save();
            return redirect('ocs')->with('success', ' Deactivated successfully');
        }
        public function activate($id){
            $ocs = OCS::find($id);
            $ocs ->status = 1;
            $ocs->save();
            return redirect('ocs')->with('success', ' Activated successfully');

        }
        public function edit($id){
        
            $ocs = OCS::find($id);
            return view('ocs.edit', compact('ocs','id'));
        }
    
        public function update(Request $request , $id){
            $this->validate($request , [
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
            $ocs = OCS::find($id);
            $ocs->first_name = $request->get('first_name');
            $ocs->last_name = $request->get('last_name');
            $ocs->save();
            return redirect('ocs')->with('success','Data Updated');
        }
        function generatePin($length = 4)
        {
            $chars = '0123456789';
            $count = mb_strlen($chars);
    
            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }
            return $result;
        }
        public function getData(){
            $policeposts = PoliceStation::all();                              
            return view('ocs.new', compact('policeposts'));
        }
}
