<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Prosecutor;
use DB;
use Auth;

class ProsecutorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        $prosecutors = DB::select('SELECT * FROM prosecutors');
        return view('prosecutor.index', ['prosecutors'=>$prosecutors]);
    }
    public function create(){
        
        return view('prosecutor.new');
    }
     //saves the details of the judge to the database
     public function store(Request $request)
     {
         $this->validate($request,[
         
             'email'=>'required|unique:prosecutors',
             'first_name'=>'required',
             'last_name'=>'required',
            //  'phone_number' => 'required|min:10|max:13|size:10|unique:judicial_officers',
            //  'type'=>'required',           
 
         ]);
 
         $pin = $this->generatePin();
         $prosecutor_id = $this->generatePin();
 
         //extends the user class
        
            $prosecutor = new Prosecutor();
            $prosecutor->password = Hash::make($pin);
            $prosecutor->prosecutor_id = $prosecutor_id;
            $prosecutor->email = $request->email;
            $prosecutor->first_name = $request->first_name;
            $prosecutor->last_name = $request->last_name;
            $prosecutor->status = 1;
            $prosecutor->save();
            $detail = "You have been added as a Prosecutor";
        $email = $request->email;
        $year = date("Y");
        $sender = "ECourtroom";
    
        Mail::send('prosecutors.emails.created', ['name' => $request->first_name, 'password' => $pin, 'detail' => $detail, 'year' => $year, 'sender' => $sender],
            function ($message) use ($email) {
                $message->from('info@ecourtroom.org', 'ECourtroom');
                $message->to($email)->subject('Prosecutor Added');
            });

         
         return redirect('/prosecutor');
        }
        public function deactivate($id){
            $prosecutor=Prosecutor::find($id);
            $prosecutor->status= 2;
            $prosecutor->save();
            return redirect('prosecutor')->with('success', ' Deactivated successfully');
        }
        public function activate($id){
            $prosecutor = Prosecutor::find($id);
            $prosecutor ->status = 1;
            $prosecutor->save();
            return redirect('prosecutor')->with('success', ' Activated successfully');

        }
        public function edit($id){
        
            $prosecutor = prosecutor::find($id);
            return view('prosecutor.edit', compact('prosecutor','id'));
        }
    
        public function update(Request $request , $id){
            $this->validate($request , [
                'first_name' => 'required',
                'last_name' => 'required',
            ]);
            $prosecutor = prosecutor::find($id);
            $prosecutor->first_name = $request->get('first_name');
            $prosecutor->last_name = $request->get('last_name');
            $prosecutor->save();
            return redirect('prosecutor')->with('success','Data Updated');
        }
        function generatePin($length = 4)
        {
            $chars = '0123456789';
            $count = mb_strlen($chars);
    
            for ($i = 0, $result = ''; $i < $length; $i++) {
                $index = rand(0, $count - 1);
                $result .= mb_substr($chars, $index, 1);
            }
            return $result;
        }
    
}


