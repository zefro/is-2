<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crime;
use App\PoliceStation;
use App\County;
use Auth;
use DB;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function dashboard()
    {
        return view('users.index');
    }
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }  
    public function showReportCrime(){
        return view('crime.new');
    }

    public function ReportCrime(Request $request){
        $this->validate($request,[
         
            // 'ob_number'=>'required',
            // 'location'=>'required',
            // 'report'=>'required',
            // 'evidence'=>'required',     
            // 'image'=>'required',
            // 'status'=>'required',

        ]);

       

        //extends the user class
        $counties = County::all();
       
        $crime = new Crime();
        // $obnumber = $this->generateOb();

        $useremail = Auth::user()->email;
        
        $crime->location=$request->input('location');
        $crime->police_station_name=$request->input('policestation');
        $crime->ob_number= $crime->location."|".$crime->police_station_name."|".date('Y-m-d-H:i:s'); 
        $crime->report=$request->input('report');
        $crime->evidence=$request->input('evidence');
        $crime->image_name=$request->input('image_name');
        $crime->status='reported';
        $crime->investigating_officer_id='0';
        $crime->ocs_id='0';
           if($request-> hasfile('image_url')){
               $file = $request->file('image_url');
               $extension = $file -> getClientOriginalExtension();
               $filename = time() . '.' . $extension;
               $file -> move('uploads/crimes/' , $filename);
               $crime->image_url= $filename;
           }else{
               return $request;
               $crime->image_url ='';
           } 
        //    function generateOb(){
        //     $obnumber = $crime->location;
        //     dd($obnumber);
        // }    
        $crime ->user_email = $useremail;
        $crime ->save();
        return redirect('/user')->with('crime',$crime);
    }
    public function getData(){
        $counties = County::all();                              
        return view('crime.new', compact('counties'));
    }

    public function getPoliceStations($id){
        $policestations = PoliceStation::where('county_code',$id)->pluck("police_station_name","police_station_code");
        return json_encode($policestations);
    }
    public function showCases(){

        $email = Auth::user()->email;
        
        $crimes = Crime::all()->where(['user_email'],$email);
        return view('users.crimes')->with('crimes', $crimes);
    }
   
}