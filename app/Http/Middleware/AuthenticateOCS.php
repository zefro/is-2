<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class AuthenticateOCS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if (!Auth::guard('ocs')->check()){
            return redirect('ocs/login')->with("verify","Please Login first");
        }
    
            return $next($request);
        }
    }
    
