<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class RedirectInvestigatingOfficerifAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard()->check()){
            return redirect('/investigatingofficer/login');
        }
        if (Auth::guard('investigatingofficer')->check()){
            return redirect('investigatingofficer/home');
        }
        return $next($request);
    }
}