<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class AuthenticateProsecutor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
    if (!Auth::guard('prosecutor')->check()){
        return redirect('prosecutor/login')->with("verify","Please Login first");
    }

        return $next($request);
    }
}
