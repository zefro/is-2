<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class AuthenticateInvestigatingOfficer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if (!Auth::guard('investigatingofficer')->check()){
            return redirect('investigatingofficer/login')->with("verify","Please Login first");
        }
    
            return $next($request);
        }
    }
    
