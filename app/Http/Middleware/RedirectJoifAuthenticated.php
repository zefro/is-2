<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class RedirectJoifAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard()->check()){
            return redirect('/judicialofficer/login');
        }
        if (Auth::guard('judicialofficer')->check()){
            return redirect('judicialofficer/home');
        }
        return $next($request);
    }
}