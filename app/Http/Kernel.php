<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'admin'=> \App\Http\Middleware\AuthenticateAdmin::class,
        'judicialofficer_auth'=> \App\Http\Middleware\AuthenticateJo::class,
        'judicialofficer_guest'=> \App\Http\Middleware\RedirectJoifAuthenticated::class,
        'prosecutor_auth'=> \App\Http\Middleware\AuthenticateProsecutor::class,
        'prosecutor_guest'=> \App\Http\Middleware\RedirectProsecutorifAuthenticated::class,
        'OCS_auth'=> \App\Http\Middleware\AuthenticateOCS::class,
        'OCS_guest'=> \App\Http\Middleware\RedirectOCSifAuthenticated::class,
        'investigating_officer_auth'=> \App\Http\Middleware\AuthenticateInvestigatingOfficer::class,
        'investigating_officer_guest'=> \App\Http\Middleware\RedirectInvestigatingOfficerifAuthenticated::class,
        // 'user'=> \App\Http\Middleware\AuthenticateUser::class,
    ];
}
