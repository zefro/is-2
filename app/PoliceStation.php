<?php

namespace App;
use App\County;

use Illuminate\Database\Eloquent\Model;

class PoliceStation extends Model
{
    public function county()
    {
        return $this->belongsTo('App\County');
    }
    protected $fillable = [
        'police_station_code','police_station_name' ,'county_code', 'county_id'
    ];
}
