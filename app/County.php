<?php

namespace App;
use App\PoliceStation;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    public function policeStation()
    {
        return $this->hasMany('App\PoliceStation');
    }
    protected $fillable = [
        'county_code','county_name'
    ];
}
