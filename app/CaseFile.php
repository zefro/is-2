<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseFile extends Model
{
    public $table = "cases";
    protected $fillable = [
        'case_id',
        'io_id',
        'ocs_id',
        'prosecutor_id',
        'jo_id',
        'witness_statement','report',
        'suspect_1_id','suspect_1_first_name','suspect_1_last_name','suspect_1_sex',
        'case_status','recomendation'
    ];
}
