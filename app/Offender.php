<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offender extends Model
{ 
    public $table = "offenders";
    protected $fillable = [
        'ob_number','national_id','first_name','last_name','sex','offender_id','plea','offence','sentence','bail'
    ];
}
