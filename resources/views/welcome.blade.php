<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" />
<link rel="stylesheet" type="text/css" href="css/homepage.css">
<header id="home">
<title>E-COURTROOM</title>
        <div class="overlay"></div>
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="javascript:void(0)">
                    <h3 class="my-heading ">E-COURTROOM</h3>
                </a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars mfa-white"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
					 <li class="nav-item">
                            <a class="nav-link" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#overview">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#overview"> Reports </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="#testimonials">Testimonial</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" href="#blog">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">Contact</a>
                        </li> -->

                    </ul>

                </div>

            </div>
        </nav>

        <div class="tophead" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7 ">
                        <h1 class="title-main wow fadeInLeft" data-wow-duration="1.5s">Online Wildlife Courtroom</h1>
                        <h3 class="subtitle-main wow fadeInUp" data-wow-duration="1.1s">An aim for wildlife justice to prevail by public effort.</h3>
						<div class="top-btn-block wow fadeInUp data-wow-duration="2.5s">
							<a href="/login" class="btn-explore ">Sign In</a>
							<a href="/register" class="btn-account ">Create Account</a>
						</div>
                    </div>
                </div>
            </div>

        </div>
        <!-- <div class="sesgoabajo"></div> -->
    </header>
    <main>
        <section class="overview-wrap" id="overview">
            <div class="container">
                <div class="contenedor">
                    <h2 class="title-overview wow fadeInUp">Publicly Available Reports</h2>
                    <p class="subtitle-overview wow fadeInUp">Below are some of the reports generated for public access by ecourtoom</p>
                    <div class="row">
                        <div class="col-md-6 col-lg-3 wow bounceInUp" data-wow-duration="1.4s">
                            <div class="overview-box mx-auto">
                                <div class="features-icons-icon d-flex">
                                    <i class="fa fa-html5 fa-5x html5 m-auto"></i>
                                </div>
                                <h5>Html 5</h5>
                                <p class=" mb-0">Lorem ipsum dolor sit amet consectetur adipiscing elit proin leo ornare!</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 wow bounceInUp" data-wow-duration="1.4s">
                            <div class="overview-box mx-auto">
                                <div class="features-icons-icon d-flex">
                                    <i class="fa fa-css3 fa-5x css3 m-auto"></i>
                                </div>
                                <h5>CSS 3</h5>
                                <p class=" mb-0">Lorem ipsum dolor sit amet consectetur adipiscing elit proin leo ornare!</p>
                            </div>
                        </div>
                       <div class="col-md-6 col-lg-3 wow bounceInUp" data-wow-duration="1.4s">
                            <div class="overview-box mx-auto">
                                <div class="features-icons-icon d-flex">
                                    <i class="fa fa-android fa-5x android m-auto"></i>
                                </div>
                                <h5>Android</h5>
                                <p class=" mb-0">Lorem ipsum dolor sit amet consectetur adipiscing elit proin leo ornare!</p>
                            </div>
                        </div>
                       <div class="col-md-6 col-lg-3 wow bounceInUp" data-wow-duration="1.4s">
                            <div class="overview-box mx-auto">
                                <div class="features-icons-icon d-flex">
                                    <i class="fa fa-drupal fa-5x drupal m-auto"></i>
                                </div>
                                <h5>Drupal</h5>
                                <p class=" mb-0">Lorem ipsum dolor sit amet consectetur adipiscing elit proin leo ornare!</p>
                            </div>
                        </div>
                    </div>

                  
                </div>
            </div>
        </section>
    </main>
    <footer class="footer bg-dark">
        <div class="container">
            <div class="row">               
                <div class="col-lg-12 text-center  my-auto ">                   
                    <p class="mb-4 mb-lg-0">© Ecourtroom 2020. All Rights Reserved.</p>
                </div>                
            </div>
        </div>
    </footer>
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
            <script>
              new WOW().init();
              </script>
    <script>
      
        $(window).on("scroll", function() {
            if ($(this).scrollTop() > 10) {
                $("nav.navbar").addClass("mybg-dark");
                $("nav.navbar").addClass("navbar-shrink");

            } else {
                $("nav.navbar").removeClass("mybg-dark");
                $("nav.navbar").removeClass("navbar-shrink");

            }

        });

        
    </script>