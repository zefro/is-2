<!DOCTYPE html>
<html>
<head>
<title>User | Dashboard </title>
@include('layouts.master')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Main Sidebar Container -->
  @include('layouts.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-8">
          <div class="col-sm-20">
            <h1>CASE NUMBER :{{$case->case_id}} </h1>
          </div>
          <div class="col-sm-10">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">           
      <div class="container">
			
				<div class="main-login main-center">

						<div class="form-group{{ $errors->has('witness_statement') ? ' has-error' : '' }}">
							<label for="witness_statement" class="cols-sm-2 control-label">Witness Statement</label>
							<div class="cols-sm-10">
                            {{$case->witness_statement}}
                            <hr/>			
							</div>
						</div>

						<div class="form-group{{ $errors->has('report') ? ' has-error' : '' }}">
							<label for="report" class="cols-sm-2 control-label">Report</label>
							<div class="cols-sm-10">
                            {{$case->report}}	
							</div>
                            <hr/>	
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_id') ? ' has-error' : '' }}">
							<label for="suspect_1_id" class="cols-sm-2 control-label">Suspect ID</label>
							<div class="cols-sm-10">
                            {{$case->suspect_1_id}}	
							</div>
                            <hr/>	
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_first_name') ? ' has-error' : '' }}">
							<label for="suspect_1_first_name" class="cols-sm-2 control-label">Suspect First Name</label>
							<div class="cols-sm-10">
                            {{$case->suspect_1_first_name}}	
							</div>
                            <hr/>	
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_last_name') ? ' has-error' : '' }}">
							<label for="suspect_1_last_name" class="cols-sm-2 control-label">Suspect Last Name</label>
							<div class="cols-sm-10">
                            {{$case->suspect_1_last_name}}	
							</div>
                            <hr/>	
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_sex') ? ' has-error' : '' }}">
							<label for="suspect_1_sex" class="cols-sm-2 control-label">Suspect Sex</label>
								<div class="cols-sm-10">
                                @if($case->suspect_1_sex == 1)
                                    MALE
                                @elseif($case->suspect_1_sex == 2)
                                    FEMALE
                                @endif
						  		</div>
                            <hr/>	
						</div>
                        @if($case->recomendation == 1)
                            <a href="{{ url('/ocs/assign/prosecutor',$case->id)}}" class="btn btn-success">PROSECUTE </a>                               
                        @elseif($case->recomendation == 2)
                            <a href="{{ url('/ocs/close/case',$case->id)}}" class="btn btn-danger">CLOSE FILE </a>
                        @endif                       
                        </div>
                        <br/>
				</div>
			
		</div>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io"> LTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  // $(function () {
  //   $("#example1").DataTable({
  //     "responsive": true,
  //     "autoWidth": false,
  //   });
    // $('#example2').DataTable({
    //   "paging": true,
    //   "lengthChange": false,
    //   "searching": false,
    //   "ordering": true,
    //   "info": true,
    //   "autoWidth": false,
    //   "responsive": true,
    // });
  });
</script>
</body>
</html>
