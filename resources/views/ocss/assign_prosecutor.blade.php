<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCS | ASSIGN </title>



		<link href="css/bootstrap.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/register.css">
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Assign | Prosecutor</title>
	</head>
	<body>

    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">
        <div class="box box-primary card card-5">
            <div class="box-header">
                
            </div>
            <div class="box-body">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>

                @endif
                <form autocomplete="off"  action="{{url('ocs/update/prosecutor', $id)}}"
                 method="post" role="form">
                 {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT"/>
                    <div class="form-group">
                        <label>Assign A Prosecutor</label>
                        <div class="form-group{{ $errors->has('prosecutor_id') ? ' has-error' : '' }}">
							<!-- <label for="location" class="cols-sm-2 control-label">Location</label> -->
								<div class="cols-sm-10">
									<select class="form-control" id="prosecutor_id" name="prosecutor_id" required>
										<option value="0" disabled="true" selected="true">--SELECT PROSECUTOR--</option>
              						@foreach ($prosecutors as $prosecutor)
              							 <option value="{{$prosecutor->prosecutor_id}}">{{$prosecutor->first_name }}{{$prosecutor->last_name }}</option>
              						@endforeach
      								</select>
						  		</div>
						</div>
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a 
                    href="/ocs/home"class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a>
                </form>
            </div>
        </div>
    </div>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	</body>
</html>