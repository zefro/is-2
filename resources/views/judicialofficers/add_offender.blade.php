<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Update | Case</title>


		<link href="css/bootstrap.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/register.css">
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="main-login main-center">
					<form class="" method="POST" 
					action="{{ url('judicialofficer/add/offender',$case->id)}}"
					>
                    {{ csrf_field() }}
					<div class="form-group{{ $errors->has('national_id') ? ' has-error' : '' }}">
							<label for="email" class="cols-sm-2 control-label">National ID</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="national_id" id="national_id" value="{{ old('national_id') }}" required autofocus placeholder="Suspect National ID"/>
                                    @if ($errors->has('national_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('national_id') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>
						<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
							<label for="email" class="cols-sm-2 control-label">First Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name') }}" required autofocus placeholder="Suspect First Name"/>
                                    @if ($errors->has('report'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('report') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
							<label for="evidence" class="cols-sm-2 control-label">Last Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="last_name" id="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Suspect Last Name"/>
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
                        </div>
                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
							<label for="sex" class="cols-sm-2 control-label">Offender Sex</label>
								<div class="cols-sm-10">
									<select class="form-control" id="sex" name="sex" required>
										<option value="0" disabled="true" selected="true">--SELECT SEX--</option>
              							<option value="male">MALE</option>
              						    <option value="female">FEMALE</option>
      								</select>
						  		</div>
						</div>
						<div class="form-group{{ $errors->has('plea') ? ' has-error' : '' }}">
							<label for="image_url" class="cols-sm-2 control-label">Plea</label>
							<div class="cols-sm-10">
								<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
               						 <input id="plea" class="form-control"  name="plea">
										@if ($errors->has('plea'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('plea') }}</strong>
                                        </span>
                                     @endif
           						 </div>
							</div>
						</br>
						<div class="form-group{{ $errors->has('offence') ? ' has-error' : '' }}">
							<label for="offence" class="cols-sm-2 control-label">Offence</label>
							<div class="cols-sm-10">
								<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
               						 <textarea id="offence" class="form-control"  name="offence"></textarea>
										@if ($errors->has('offence'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('offence') }}</strong>
                                        </span>
                                     @endif
           						 </div>
							</div>
							</br>
							<div class="form-group{{ $errors->has('sentence') ? ' has-error' : '' }}">
								<label for="sentence" class="cols-sm-2 control-label">Sentence</label>
							<div class="cols-sm-10">
								<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
               						 <textarea id="sentence" class="form-control" name="sentence"></textarea>
										@if ($errors->has('sentence'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sentence') }}</strong>
                                        </span>
                                     @endif
           						 </div>
							</div>
							</br>
							<div class="form-group{{ $errors->has('bail') ? ' has-error' : '' }}">
							<label for="bail" class="cols-sm-2 control-label">Bail</label>
							<div class="cols-sm-10">
								<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
               						 <input id="bail" class="form-control" name="bail">
										@if ($errors->has('bail'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('bail') }}</strong>
                                        </span>
                                     @endif
           						 </div>
							</div>
							<br/>
                        <div class="form-group">   
                            <button type="submit" class="btn btn-danger btn-lg btn-block">
                                Add Offender
                            </button>        
                        </div>
						
					</form>
				</div>
			</div>
		</div>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
	var county_id = "no data"
	jQuery(document).ready(function(){

		
		jQuery('select[name="location"]').on('change',function(){
			
			var county_id = jQuery(this).val();
			if(county_id){
				
				jQuery.ajax({
					url :'/user/report/' + county_id,
					type : "GET",
					dataType : "json",
					success:function(data){
					// alert(data);
						jQuery('select[name="policestation"]').empty();
						jQuery.each(data, function(key,value){
							
							$('select[name="policestation"]').append('<option value="' +key+'">'+value+'</option>');
							
						});
					}
				});
			}
			else
			{
				$('select[name="policestation"]').empty();
				
			}
		});
		$ob = $('#location').val().append($('#policestation'));
	});
	</script>
	</body>
</html>