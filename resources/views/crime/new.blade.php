<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>User | Report Crime </title>


		<link href="css/bootstrap.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/register.css">
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Report Crime </title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="main-login main-center">
					<form class="" method="POST" action="{{ route('save.crime') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
						<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
							<label for="location" class="cols-sm-2 control-label">Location</label>
								<div class="cols-sm-10">
									<select class="form-control" id="location" name="location" required>
										<option value="0" disabled="true" selected="true">--SELECT COUNTY--</option>
              						@foreach ($counties as $county)
              							 <option value="{{$county->county_code}}">{{$county->county_name }}</option>
              						@endforeach
      								</select>
						  		</div>
						</div>

						<div class="form-group{{ $errors->has('policestation') ? ' has-error' : '' }}">
							<label for="policestation" class="cols-sm-2 control-label">Police Post</label>
								<div class="cols-sm-10">
									<select class="form-control" id="policestation" name="policestation" required>
						 				 <option  selected="false">--SELECT POLICE POST--</option>
									</select>
								</div>
						</div>

						<div class="form-group{{ $errors->has('report') ? ' has-error' : '' }}">
							<label for="email" class="cols-sm-2 control-label">Report</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="report" id="report" value="{{ old('report') }}" required autofocus placeholder="Write a detailed Report of the crime"/>
                                    @if ($errors->has('report'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('report') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('evidence') ? ' has-error' : '' }}">
							<label for="evidence" class="cols-sm-2 control-label">Evidence</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="evidence" id="evidence" value="{{ old('evidence') }}" required autofocus placeholder="Key words of the evidence e.g. Charcoal Burning"/>
                                    @if ($errors->has('evidence'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('evidence') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
                        </div>
                        <div class="form-group{{ $errors->has('image_name') ? ' has-error' : '' }}">
							<label for="image_name" class="cols-sm-2 control-label">Image Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="image_name" id="image_name" value="{{ old('image_name') }}" required autofocus placeholder="Image Name"/>
                                    @if ($errors->has('image_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image_name') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						<div class="form-group{{ $errors->has('image_url') ? ' has-error' : '' }}">
							<label for="image_url" class="cols-sm-2 control-label">Choose Image</label>
							<div class="cols-sm-10">
								<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
               						 <input id="image_url" class="form-control" type="file" name="image_url">
										
										@if ($errors->has('image_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image_name') }}</strong>
                                        </span>
                                     @endif
           						 </div>
							</div>
							</br>

                        <div class="form-group">   
                            <button type="submit" class="btn btn-danger btn-lg btn-block">
                                Report Crime
                            </button>        
                        </div>
						
					</form>
				</div>
			</div>
		</div>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
	var county_id = "no data"
	jQuery(document).ready(function(){

		
		jQuery('select[name="location"]').on('change',function(){
			
			var county_id = jQuery(this).val();
			if(county_id){
				
				jQuery.ajax({
					url :'/user/report/' + county_id,
					type : "GET",
					dataType : "json",
					success:function(data){
					// alert(data);
						jQuery('select[name="policestation"]').empty();
						jQuery.each(data, function(key,value){
							
							$('select[name="policestation"]').append('<option value="' +key+'">'+value+'</option>');
							
						});
					}
				});
			}
			else
			{
				$('select[name="policestation"]').empty();
				
			}
		});
		$ob = $('#location').val().append($('#policestation'));
	});
	</script>
	</body>
</html>