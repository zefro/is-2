<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>New | Investigating Officer </title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/register.css">
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="main-login main-center">
					<form class="" role="form" method="POST" action="{{url('investigatingofficer/submit/case', $id)}}">
                    {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT"/>
						<div class="form-group{{ $errors->has('witness_statement') ? ' has-error' : '' }}">
							<label for="witness_statement" class="cols-sm-2 control-label">Witness Statement</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"></span>
                                    <textarea type="text" class="form-control" name="witness_statement" id="witness_statement" value="{{ old('witness_statement') }}" required autofocus placeholder="Witness Statement"></textarea>
                                    @if ($errors->has('witness_statement'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('witness_statement') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('report') ? ' has-error' : '' }}">
							<label for="report" class="cols-sm-2 control-label">Report</label>
							<div class="cols-sm-10">
								<div class="input-group">
                                <span class="input-group-addon"></span>
                                    <textarea type="text" class="form-control" name="report" id="report" value="{{ old('report') }}" required autofocus placeholder="Report on crime"></textarea>
                                    @if ($errors->has('report'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('report') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_id') ? ' has-error' : '' }}">
							<label for="suspect_1_id" class="cols-sm-2 control-label">Suspect ID</label>
							<div class="cols-sm-10">
								<div class="input-group">
                                <span class="input-group-addon"></span>
                                    <input type="text" class="form-control" name="suspect_1_id" id="suspect_1_id" value="{{ old('suspect_1_id') }}" required autofocus placeholder="Enter Suspect ID"/>
                                    @if ($errors->has('suspect_1_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('suspect_1_id') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_first_name') ? ' has-error' : '' }}">
							<label for="suspect_1_first_name" class="cols-sm-2 control-label">Suspect First Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
                                <span class="input-group-addon"></span>
                                    <input type="text" class="form-control" name="suspect_1_first_name" id="suspect_1_first_name" value="{{ old('suspect_1_first_name') }}" required autofocus placeholder="Enter Suspect First Name"/>
                                    @if ($errors->has('suspect_1_first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('suspect_1_first_name') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_last_name') ? ' has-error' : '' }}">
							<label for="suspect_1_last_name" class="cols-sm-2 control-label">Suspect Last Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
                                <span class="input-group-addon"></span>
                                    <input type="text" class="form-control" name="suspect_1_last_name" id="suspect_1_last_name" value="{{ old('suspect_1_last_name') }}" required autofocus placeholder="Enter Suspect Last Name"/>
                                    @if ($errors->has('suspect_1_last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('suspect_1_last_name') }}</strong>
                                        </span>
                                     @endif
								</div>
							</div>
						</div>
                        <div class="form-group{{ $errors->has('suspect_1_sex') ? ' has-error' : '' }}">
							<label for="suspect_1_sex" class="cols-sm-2 control-label">Select Sex</label>
								<div class="cols-sm-10">
									<select class="form-control" id="location" name="suspect_1_sex" required>
										<option value="0" disabled="true" selected="true">--SELECT SEX--</option>
              							<option value="1">MALE</option>
              						    <option value="2">FEMALE</option>
      								</select>
						  		</div>
						</div>
						<div class="form-group{{ $errors->has('recomendation') ? ' has-error' : '' }}">
							<label for="recomendation" class="cols-sm-2 control-label">Recomendation</label>
								<div class="cols-sm-10">
									<select class="form-control" id="recomendation" name="recomendation" required>
										<option value="0" disabled="true" selected="true">--SELECT RECOMENDATION--</option>
              							<option value="1">PROSECUTE</option>
              						    <option value="2">CLOSE FILE</option>
      								</select>
						  		</div>
						</div>
						
                        <div class="form-group">
                            
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Submit Report
                                </button>
                            
                        </div>
						
					</form>
				</div>
			</div>
		</div>

		 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	</body>
</html> 