<!DOCTYPE html>
<html>
<title>Admin | CASES </title>
<head>
@include('layouts.master')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Main Sidebar Container -->
  @include('layouts.sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>CASES MANAGEMENT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">

            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                              
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead> 
                  <tr>
                    <th>OB NUMBER</th>
                    <th>WITNESS STATEMENT</th>
                    <th>REPORT</th>
                    <th>STATUS</th>
                   
                  </tr>
                 
                  @foreach($cases as $case)
                  <tr>
                    <td>{{ $case->case_id }}</td>
                    <td>
                    @if ($case->witness_statement == "0")
                    <a >Statement Not Available</a>
                    @elseif ($case->witness_statement != "0")
                    <a >{{$case->witness_statement}} </a>                   
                    @endif
                    </td>
                    <td>
                    @if ($case->report == "0")
                    <a >Report Not Available</a>
                    @elseif ($case->report != "0")
                    <a >{{$case->report}} </a>                   
                    @endif
                    </td>
                    <td>
                    @if ($case->case_status == "reported")
                    <a href="#" class="btn btn-danger">Not Assigned</a>
                    @elseif ($case->case_status == "assigned to io")
                    <a href="#" class="btn btn-success">Assignned </a>
                    @elseif ($case->case_status == "recomendation from io")
                    <a href="#" class="btn btn-info">View Recomendation (open crime)</a>
                    @elseif ($case->case_status == "assigned to prosecutor")
                    <a href="#" class="btn btn-info">View Recomendation (prosecutor)</a>
                    @elseif ($case->case_status == "in hearing")
                    <a href="#" class="btn btn-info">In Hearing</a>
                    @elseif ($case->case_status == "closed")
                    <a href="#"class="btn btn-danger">Case Closed</a>
                    @endif
                    </td>
                </tr>
                @endforeach
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  // $(function () {
  //   $("#example1").DataTable({
  //     "responsive": true,
  //     "autoWidth": false,
  //   });
    // $('#example2').DataTable({
    //   "paging": true,
    //   "lengthChange": false,
    //   "searching": false,
    //   "ordering": true,
    //   "info": true,
    //   "autoWidth": false,
    //   "responsive": true,
    // });
  });
</script>
</body>
</html>
