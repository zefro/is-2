<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crimes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ob_number');
            $table->string('location');
            $table->string('police_station_name');
            $table->string('investigating_officer_id')=="0";
            $table->string('ocs_id')=="0";
            $table->string('report');
            $table->string('evidence');
            $table->string('image_name');
            $table->string('image_url');
            $table->string('status') == "1";
            $table->string('user_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crimes');
    }
}
