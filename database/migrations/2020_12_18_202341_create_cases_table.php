<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('case_id');
            $table->string('io_id');
            $table->string('ocs_id');
            $table->string('prosecutor_id');
            $table->string('jo_id');
            $table->string('witness_statement');
            $table->longText('report');
            $table->string('suspect_1_id');
            $table->string('suspect_1_first_name');
            $table->string('suspect_1_last_name');
            $table->string('suspect_1_sex');
            $table->string('case_status');
            $table->string('recomendation');
            $table->rememberToken();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
