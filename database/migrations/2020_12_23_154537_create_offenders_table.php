<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offenders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ob_number');
            $table->string('national_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('sex');
            $table->string('offender_id')->unique();
            $table->string('plea');
            $table->longText('offence');
            $table->longText('sentence');
            $table->string('bail');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offenders');
    }
}
