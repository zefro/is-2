<?php

use Illuminate\Database\Seeder;

class PoliceStationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //NAIROBI COUNTY - COUNTY CODE NRB
        App\PoliceStation::create([
            'police_station_code' => 'NRBKA',
            'police_station_name' =>'Kangemi Police Station',
            'county_code' => '47',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'NRBMU',
            'police_station_name' =>'Mutuini Police Station',
            'county_code' => '47',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'NRBWA',
            'police_station_name' =>'Waithaka Police Station',
            'county_code' => '47',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'NRBLA',
            'police_station_name' =>'Langata Police Station',
            'county_code' => '47',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'NRBCA',
            'police_station_name' =>'California Police Station',
            'county_code' => '47',
        ]);

        //MOMBASA COUNTY - COUNTY CODE MSA
        App\PoliceStation::create([
            'police_station_code' => 'MSAMB',
            'police_station_name' =>'Mbaraki Lines Police Station',
            'county_code' => '1',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'MSATO',
            'police_station_name' =>'Tononoka Police Station',
            'county_code' => '1',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'MSAKO',
            'police_station_name' =>'Kongowea Market Police Station',
            'county_code' => '1',
        ]);

        //KWALE COUNTY- COUNTY CODE KWA
        App\PoliceStation::create([
            'police_station_code' => 'KWABO',
            'police_station_name' =>'Bofu Police Station',
            'county_code' => '2',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'KWAND',
            'police_station_name' =>'Ndavaya Police Station',
            'county_code' => '2',
        ]);

        App\PoliceStation::create([
            'police_station_code' => 'KWAMA',
            'police_station_name' =>'Mavitini Police Station',
            'county_code' => '2',
        ]);

        //KILIFI COUNTY - KIL
        App\PoliceStation::create([
            'police_station_code' => 'KILNG',
            'police_station_name' =>'Ngerenya Police Station',
            'county_code' => '3',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'KILMA',
            'police_station_name' =>'Matsangoni Police Station',
            'county_code' => '3',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'KILNG',
            'police_station_name' =>'Ngerenya Police Station',
            'county_code' => '3',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'KILGO',
            'police_station_name' =>'Gotani Police Station',
            'county_code' => '3',
        ]);

        //TANA RIVER COUNTY - TAN
        App\PoliceStation::create([
            'police_station_code' => 'TANCHA',
            'police_station_name' =>'Chardende Police Station',
            'county_code' => '4',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'TANASA',
            'police_station_name' =>'Asako Police Station',
            'county_code' => '4',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'TANWAL',
            'police_station_name' =>'Asako Police Station',
            'county_code' => '4',
        ]);

        //LAMU COUNTY LAM
        App\PoliceStation::create([
            'police_station_code' => 'LAMMO',
            'police_station_name' =>'Mokowe Police Station',
            'county_code' => '5',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'LAMHI',
            'police_station_name' =>'Hindi Police Station',
            'county_code' => '5',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'LAMKA',
            'police_station_name' =>'Kapu Police Station',
            'county_code' => '5',
        ]);

        //TAITA TAVENTA COUNTY - TATA
        App\PoliceStation::create([
            'police_station_code' => 'TATAMA',
            'police_station_name' =>'Maungu Police Station',
            'county_code' => '6',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'TATAKA',
            'police_station_name' =>'Kasigau Police Station',
            'county_code' => '6',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'TATATA',
            'police_station_name' =>'Tausa Police Station',
            'county_code' => '6',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'TATAMG',
            'police_station_name' =>'Mghange Police Station',
            'county_code' => '6',
        ]);

        //WAJIR COUNTY WAJ
        App\PoliceStation::create([
            'police_station_code' => 'WAJKH',
            'police_station_name' =>'Khorof Harar Police Station',
            'county_code' => '8',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'WAJDI',
            'police_station_name' =>'Diff Police Station',
            'county_code' => '8',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'WAJGH',
            'police_station_name' =>'Gherile Police Station',
            'county_code' => '8',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'WAJLE',
            'police_station_name' =>'Leheley Police Station',
            'county_code' => '8',
        ]);

        //GARISSA COUNTY GAR
        App\PoliceStation::create([
            'police_station_code' => 'GARSA',
            'police_station_name' =>'Sankuni Police Station',
            'county_code' => '7',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'GARIF',
            'police_station_name' =>'Iftin Police Station',
            'county_code' => '7',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'GARSAN',
            'police_station_name' =>'Sangailu Police Station',
            'county_code' => '7',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'GARDU',
            'police_station_name' =>'Dujis Police Station',
            'county_code' => '7',
        ]);

            //MANDERA COUNTY MAN
            App\PoliceStation::create([
                'police_station_code' => 'MANAR',
                'police_station_name' =>'Arabia Police Station',
                'county_code' => '9',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'MANKH',
                'police_station_name' =>'Khalalio Police Station',
                'county_code' => '9',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'MANAS',
                'police_station_name' =>'Ashabito Police Station',
                'county_code' => '9',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MANSA',
                'police_station_name' =>'Sala Police Station',
                'county_code' => '9',
            ]);

            //KISUMU COUNTY --KIS
            App\PoliceStation::create([
                'police_station_code' => 'KISRA',
                'police_station_name' =>'Ratta Police Station',
                'county_code' => '42',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KISRE',
                'police_station_name' =>'Reru Police Station',
                'county_code' => '42',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KISOB',
                'police_station_name' =>'Obola Police Station',
                'county_code' => '42',
            ]);

            //HOMA BAY COUNTY HOM
            App\PoliceStation::create([
                'police_station_code' => 'HOMNY',
                'police_station_name' =>'Nyangiela Police Station',
                'county_code' => '43',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'HOMOT',
                'police_station_name' =>'Othoro Police Station',
                'county_code' => '43',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'HOMRI',
                'police_station_name' =>'Ringa Police Station',
                'county_code' => '43',
            ]);

            //MIGORI COUNTY MIG
            App\PoliceStation::create([
                'police_station_code' => 'MIGKA',
                'police_station_name' =>'Kabado Police Station',
                'county_code' => '44',
            ]);

            //SIAYA COUNTY SIY
            App\PoliceStation::create([
                'police_station_code' => 'SIYND',
                'police_station_name' =>'Ndori Police Station',
                'county_code' => '41',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'SIYMA',
                'police_station_name' =>'Madiany Police Station',
                'county_code' => '41',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'SIYMAN',
                'police_station_name' =>'Manyuanda Police Station',
                'county_code' => '41',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'SIYWA',
                'police_station_name' =>'Wangarot Police Station',
                'county_code' => '41',
            ]);

            //NYAMIRA COUNTY NYA
            App\PoliceStation::create([
                'police_station_code' => 'NYANYA',
                'police_station_name' =>'Nyamaiya Police Station',
                'county_code' => '46',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NYAKI',
                'police_station_name' =>'Kiambere Police Station',
                'county_code' => '46',
            ]);

            //KISII COUNTY KISI
            App\PoliceStation::create([
                'police_station_code' => 'KISISU',
                'police_station_name' =>'Suneka Police Station',
                'county_code' => '45',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KISIIG',
                'police_station_name' =>'Igonga Police Station',
                'county_code' => '45',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KISIKE',
                'police_station_name' =>'Kerina Police Station',
                'county_code' => '45',
            ]);

            //EMBU COUNTY EMB
            App\PoliceStation::create([
                'police_station_code' => 'EMBIS',
                'police_station_name' =>'Ishiara Police Station',
                'county_code' => '14',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'EMBKI',
                'police_station_name' =>'Kianjokoma Police Station',
                'county_code' => '14',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'EMBKIB',
                'police_station_name' =>'Kibugu Police Station',
                'county_code' => '14',
            ]);

            //MARSABIT MAR

            App\PoliceStation::create([
                'police_station_code' => 'MARJA',
                'police_station_name' =>'Jaldesa Police Station',
                'county_code' => '10',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MARKA',
                'police_station_name' =>'Karare Police Station',
                'county_code' => '10',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MARDA',
                'police_station_name' =>'Dabel Police Station',
                'county_code' => '10',
            ]);


            //ISIOLO ISI
            App\PoliceStation::create([
                'police_station_code' => 'ISIKI',
                'police_station_name' =>'Kinna Police Station',
                'county_code' => '11',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'ISIKU',
                'police_station_name' =>'Kula Mawe Police Station',
                'county_code' => '11',
            ]);

            //THARAKA NITHI - THA
            App\PoliceStation::create([
                'police_station_code' => 'THANK',
                'police_station_name' =>'Nkondi Police Station',
                'county_code' => '13',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'THACH',
                'police_station_name' =>'Chiakariga Police Station',
                'county_code' => '13',
            ]);


            //MACHAKOS  -- MAC
            App\PoliceStation::create([
                'police_station_code' => 'MACNK',
                'police_station_name' =>'Konza Police Station',
                'county_code' => '16',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MACKI',
                'police_station_name' =>'Kithyhoko Police Station',
                'county_code' => '16',
            ]);

            //KITUI KIT 
            App\PoliceStation::create([
                'police_station_code' => 'KITMI',
                'police_station_name' =>'Miambani Police Station',
                'county_code' => '15',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KITKA',
                'police_station_name' =>'Katulani Police Station',
                'county_code' => '15',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KITIK',
                'police_station_name' =>'Ikanga Police Station',
                'county_code' => '15',
            ]);

            //MAKUENI MAK
            App\PoliceStation::create([
                'police_station_code' => 'MAKKYA',
                'police_station_name' =>'Kyambeke  Police Station',
                'county_code' => '17',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MAKMU',
                'police_station_name' =>'Kyambeke  Police Station',
                'county_code' => '17',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MAKTA',
                'police_station_name' =>'Tawa  Police Station',
                'county_code' => '17',
            ]);

            //MERU MER
            App\PoliceStation::create([
                'police_station_code' => 'MERKI',
                'police_station_name' =>'Kiolo  Police Station',
                'county_code' => '12',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MERMU',
                'police_station_name' =>'Muthara  Police Station',
                'county_code' => '12',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'MERMUL',
                'police_station_name' =>'Mula  Police Station',
                'county_code' => '12',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'MERLO',
                'police_station_name' =>'Lowangila  Police Station',
                'county_code' => '12',
            ]);

            //NYANDARUA - NYA
            App\PoliceStation::create([
                'police_station_code' => 'NYASH',
                'police_station_name' =>'Shamata  Police Station',
                'county_code' => '18',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'NYAGW',
                'police_station_name' =>'Gwakungu  Police Station',
                'county_code' => '18',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'NYAGA',
                'police_station_name' =>'Gatimu  Police Station',
                'county_code' => '18',
            ]);

            //NYERI - NYE
            App\PoliceStation::create([
                'police_station_code' => 'NYECH',
                'police_station_name' =>'Chinga  Police Station',
                'county_code' => '19',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NYEMU',
                'police_station_name' =>'Muyange  Police Station',
                'county_code' => '19',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NYEKA',
                'police_station_name' =>'Kamakwa  Police Station',
                'county_code' => '19',
            ]);

            //KIRINYAGA - KIR
            App\PoliceStation::create([
                'police_station_code' => 'KIRKA',
                'police_station_name' =>'Karaine  Police Station',
                'county_code' => '20',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KIRKAG',
                'police_station_name' =>'Kagumo  Police Station',
                'county_code' => '20',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KIRMU',
                'police_station_name' =>'Mutitu  Police Station',
                'county_code' => '20',
            ]);

            //MURANGA  - MUR

            App\PoliceStation::create([
                'police_station_code' => 'MURKE',
                'police_station_name' =>'Kenol  Police Station',
                'county_code' => '21',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MURIC',
                'police_station_name' =>'Ichagaki B.  Police Station',
                'county_code' => '21',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'MURKI',
                'police_station_name' =>'Kirigo  Police Station',
                'county_code' => '21',
            ]);

            //KIAMBU KIA
            App\PoliceStation::create([
                'police_station_code' => 'KIAKA',
                'police_station_name' =>'Kanyariri  Police Station',
                'county_code' => '22',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'KIAMU',
                'police_station_name' =>'Muguga  Police Station',
                'county_code' => '22',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'KIAKI',
                'police_station_name' =>'Kibiku  Police Station',
                'county_code' => '22',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'KIAUT',
                'police_station_name' =>'Uthiru  Police Station',
                'county_code' => '22',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KIAME',
                'police_station_name' =>'Membley  Police Station',
                'county_code' => '22',
            ]);

            //TURKANA - TUR
            App\PoliceStation::create([
                'police_station_code' => 'TURKA',
                'police_station_name' =>'Kalokol  Police Station',
                'county_code' => '23',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'TURKAT',
                'police_station_name' =>'Katilu  Police Station',
                'county_code' => '23',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'TURLOK',
                'police_station_name' =>'Lokichar  Police Station',
                'county_code' => '23',
            ]);

            //WEST POKOT COUNTY -WES
            App\PoliceStation::create([
                'police_station_code' => 'WESKA',
                'police_station_name' =>'Kanyarkwat  Police Station',
                'county_code' => '24',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'WESKE',
                'police_station_name' =>'Keringet  Police Station',
                'county_code' => '24',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'WESOR',
                'police_station_name' =>'Ortum  Police Station',
                'county_code' => '24',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'WESCH',
                'police_station_name' =>'Chepareria  Police Station',
                'county_code' => '24',
            ]);

            //SAMBURU -SAM
            App\PoliceStation::create([
                'police_station_code' => 'SAMPO',
                'police_station_name' =>'Polo  Police Station',
                'county_code' => '25',
            ]);


            //TRANS NZOIA - TRA
            App\PoliceStation::create([
                'police_station_code' => 'TRAKI',
                'police_station_name' =>'Kinyoro  Police Station',
                'county_code' => '26',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'TRAMA',
                'police_station_name' =>'Matisi  Police Station',
                'county_code' => '26',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'TRASA',
                'police_station_name' =>'Saboti  Police Station',
                'county_code' => '26',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'TRABI',
                'police_station_name' =>'Bikeke  Police Station',
                'county_code' => '26',
            ]);

            //UASIN GISHU - UAS
            App\PoliceStation::create([
                'police_station_code' => 'UASOS',
                'police_station_name' =>'Osoronga  Police Station',
                'county_code' => '27',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'UASBA',
                'police_station_name' =>'Baharini  Police Station',
                'county_code' => '27',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'UASKA',
                'police_station_name' =>'Kapyemit  Police Station',
                'county_code' => '27',
            ]);


            //ELGEYO MARAKWET - ELG
            App\PoliceStation::create([
                'police_station_code' => 'ELGKA',
                'police_station_name' =>'Kapteren  Police Station',
                'county_code' => '28',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'ELGSE',
                'police_station_name' =>'Sergoit  Police Station',
                'county_code' => '28',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'ELGAN',
                'police_station_name' =>'Anin  Police Station',
                'county_code' => '28',
            ]);

            //NANDI - NAN
            App\PoliceStation::create([
                'police_station_code' => 'NANCH',
                'police_station_name' =>'Chemomi  Police Station',
                'county_code' => '29',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'NANSI',
                'police_station_name' =>'Siret  Police Station',
                'county_code' => '29',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NANTA',
                'police_station_name' =>'Taito  Police Station',
                'county_code' => '29',
            ]);

            //BARINGO - BAR
            App\PoliceStation::create([
                'police_station_code' => 'BARKA',
                'police_station_name' =>'Kaptimbor  Police Station',
                'county_code' => '30',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BAROR',
                'police_station_name' =>'Orokwo  Police Station',
                'county_code' => '30',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BARKAB',
                'police_station_name' =>'Kabasis  Police Station',
                'county_code' => '30',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BARKAP',
                'police_station_name' =>'Kaplelwa  Police Station',
                'county_code' => '30',
            ]);

            //LAIKIPIA - LAI
            App\PoliceStation::create([
                'police_station_code' => 'LAIKA',
                'police_station_name' =>'Kariunga  Police Station',
                'county_code' => '31',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'LAIMU',
                'police_station_name' =>'Muthaiga  Police Station',
                'county_code' => '31',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'LAIMUR',
                'police_station_name' =>'Muramati  Police Station',
                'county_code' => '31',
            ]);

            //NAKURU NAK 
            App\PoliceStation::create([
                'police_station_code' => 'NAKFL',
                'police_station_name' =>'Flamingo  Police Station',
                'county_code' => '32',
            ]);
            
            App\PoliceStation::create([
                'police_station_code' => 'NAKRH',
                'police_station_name' =>'Rhino  Police Station',
                'county_code' => '32',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NAKSO',
                'police_station_name' =>'Soilo  Police Station',
                'county_code' => '32',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NAKJO',
                'police_station_name' =>'Jogoo  Police Station',
                'county_code' => '32',
            ]);

            //NAROK -NAR 
            App\PoliceStation::create([
                'police_station_code' => 'NARMW',
                'police_station_name' =>'Mwamba  Police Station',
                'county_code' => '33',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NARLO',
                'police_station_name' =>'London  Police Station',
                'county_code' => '33',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'NAROL',
                'police_station_name' =>'Olokirikirai  Police Station',
                'county_code' => '33',
            ]);

            //KAJIADO - KAJ
            App\PoliceStation::create([
                'police_station_code' => 'KAJCE',
                'police_station_name' =>'Central  Police Station',
                'county_code' => '34',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KAJEN',
                'police_station_name' =>'Enkaroni  Police Station',
                'county_code' => '34',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KAJNG',
                'police_station_name' =>'Ngatatoek  Police Station',
                'county_code' => '34',
            ]);
            
            //KERICHO - KER
            App\PoliceStation::create([
                'police_station_code' => 'KERTE',
                'police_station_name' =>'Tendwet  Police Station',
                'county_code' => '35',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KERRO',
                'police_station_name' =>'Roret  Police Station',
                'county_code' => '35',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KERNG',
                'police_station_name' =>'Ngoina  Police Station',
                'county_code' => '35',
            ]);

            //BOMET - BOM
            App\PoliceStation::create([
                'police_station_code' => 'BOMMU',
                'police_station_name' =>'Mugango  Police Station',
                'county_code' => '36',
            ]);

            App\PoliceStation::create([
                'police_station_code' => 'BOMND',
                'police_station_name' =>'Ndarawetta  Police Station',
                'county_code' => '36',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BOMKA',
                'police_station_name' =>'Kapkoros  Police Station',
                'county_code' => '36',
            ]);

            //BUNGOMA -BUN
            App\PoliceStation::create([
                'police_station_code' => 'BUNND',
                'police_station_name' =>'Ndengelwa  Police Station',
                'county_code' => '39',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BUNNZ',
                'police_station_name' =>'Nzoia  Police Station',
                'county_code' => '39',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BUNSA',
                'police_station_name' =>'Sango  Police Station',
                'county_code' => '39',
            ]);

            //BUSIA - BUS
            App\PoliceStation::create([
                'police_station_code' => 'BUSGA',
                'police_station_name' =>'Ganjala  Police Station',
                'county_code' => '40',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BUSMU',
                'police_station_name' =>'Muloghoni  Police Station',
                'county_code' => '40',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'BUSAT',
                'police_station_name' =>'Aterait  Police Station',
                'county_code' => '40',
            ]);

            //KAKAMEGA - KAK
            App\PoliceStation::create([
                'police_station_code' => 'KAKKA',
                'police_station_name' =>'Kari  Police Station',
                'county_code' => '37',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KAKSH',
                'police_station_name' =>'Shirere  Police Station',
                'county_code' => '37',
            ]);
            App\PoliceStation::create([
                'police_station_code' => 'KAKMA',
                'police_station_name' =>'Matioli  Police Station',
                'county_code' => '37',
            ]);


           //VIHIGA - VIH 
           App\PoliceStation::create([
            'police_station_code' => 'VIHMA',
            'police_station_name' =>'Magadi  Police Station',
            'county_code' => '38',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'VIHMB',
            'police_station_name' =>'Mbaja  Police Station',
            'county_code' => '38',
        ]);
        App\PoliceStation::create([
            'police_station_code' => 'VIHKI',
            'police_station_name' =>'Kigama  Police Station',
            'county_code' => '38',
        ]);

    }
}
