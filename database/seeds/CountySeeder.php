<?php

use Illuminate\Database\Seeder;

class CountySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\County::create([
            'county_code' => '1',
            'county_name' => 'Mombasa',
        ]);
        App\County::create([
            'county_code' => '2',
            'county_name' => 'Kwale',
        ]);
        App\County::create([
            'county_code' => '3',
            'county_name' => 'Kilifi',
        ]);
        App\County::create([
            'county_code' => '4',
            'county_name' => 'Tana River',
        ]);
        App\County::create([
            'county_code' => '5',
            'county_name' => 'Lamu',
        ]);
        App\County::create([
            'county_code' => '6',
            'county_name' => 'Taita-Taveta',
        ]);
        App\County::create([
            'county_code' => '7',
            'county_name' => 'Garissa',
        ]);
        App\County::create([
            'county_code' => '8',
            'county_name' => 'Wajir',
        ]);
        App\County::create([
            'county_code' => '9',
            'county_name' => 'Mandera',
        ]);
        App\County::create([
            'county_code' => '10',
            'county_name' => 'Marsabit',
        ]);
        App\County::create([
            'county_code' => '11',
            'county_name' => 'Isiolo',
        ]);
        App\County::create([
            'county_code' => '12',
            'county_name' => 'Meru',
        ]);
        App\County::create([
            'county_code' => '13',
            'county_name' => 'Tharaka-Nithi',
        ]);
        App\County::create([
            'county_code' => '14',
            'county_name' => 'Embu',
        ]);
        App\County::create([
            'county_code' => '15',
            'county_name' => 'Kitui',
        ]);
        App\County::create([
            'county_code' => '16',
            'county_name' => 'Machakos',
        ]);
        App\County::create([
            'county_code' => '17',
            'county_name' => 'Makueni',
        ]);
        App\County::create([
            'county_code' => '18',
            'county_name' => 'Nyandarua',
        ]);
        App\County::create([
            'county_code' => '19',
            'county_name' => 'Nyeri',
        ]);
        App\County::create([
            'county_code' => '20',
            'county_name' => 'Kirinyaga',
        ]);
        App\County::create([
            'county_code' => '21',
            'county_name' => 'Muranga',
        ]);
        App\County::create([
            'county_code' => '22',
            'county_name' => 'Kiambu',
        ]);
        App\County::create([
            'county_code' => '23',
            'county_name' => 'Turkana',
        ]);
        App\County::create([
            'county_code' => '24',
            'county_name' => 'West Pokot',
        ]);
        App\County::create([
            'county_code' => '25',
            'county_name' => 'Samburu',
        ]);
        App\County::create([
            'county_code' => '26',
            'county_name' => 'Trans Nzoia',
        ]);
        App\County::create([
            'county_code' => '27',
            'county_name' => 'Uasin Gishu',
        ]);
        App\County::create([
            'county_code' => '28',
            'county_name' => 'Elgeyo Marakwet',
        ]);
        App\County::create([
            'county_code' => '29',
            'county_name' => 'Nandi',
        ]);
        App\County::create([
            'county_code' => '30',
            'county_name' => 'Baringo',
        ]);
        App\County::create([
            'county_code' => '31',
            'county_name' => 'Laikipia',
        ]);
        App\County::create([
            'county_code' => '32',
            'county_name' => 'Nakuru',
        ]);
        App\County::create([
            'county_code' => '33',
            'county_name' => 'Narok',
        ]);
        App\County::create([
            'county_code' => '34',
            'county_name' => 'Kajiado',
        ]);
        App\County::create([
            'county_code' => '35',
            'county_name' => 'Kericho',
        ]);
        App\County::create([
            'county_code' => '36',
            'county_name' => 'Bomet',
        ]);
        App\County::create([
            'county_code' => '37',
            'county_name' => 'Kakamega',
        ]);
        App\County::create([
            'county_code' => '38',
            'county_name' => 'Vihiga',
        ]);
        App\County::create([
            'county_code' => '39',
            'county_name' => 'Bungoma',
        ]);
        App\County::create([
            'county_code' => '40',
            'county_name' => 'Busia',
        ]);
        App\County::create([
            'county_code' => '41',
            'county_name' => 'Siaya',
        ]);
        App\County::create([
            'county_code' => '42',
            'county_name' => 'Kisumu',
        ]);
        App\County::create([
            'county_code' => '43',
            'county_name' => 'Homa Bay',
        ]);
        App\County::create([
            'county_code' => '44',
            'county_name' => 'Migori',
        ]);
        App\County::create([
            'county_code' => '45',
            'county_name' => 'Kisii',
        ]);
        App\County::create([
            'county_code' => '46',
            'county_name' => 'Nyamira',
        ]);
        App\County::create([
            'county_code' => '47',
            'county_name' => 'Nairobi',
        ]);
    }
}
