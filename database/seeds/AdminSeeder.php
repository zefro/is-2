<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Admin::create([
            'first_name' => 'admin',
            'last_name' =>'admin',
            'email' => 'ecourtroom@gmail.org',
            'password' =>Hash::make('ecourtroom!2020')
        ]);
    }
}
