<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome');});


Auth::routes();

    //ADMIN
    
    Route::get('admin/login', 'Auth\AdminLoginController@LoginForm')->name('admin.login.form');
    Route::post('admin/login', 'Auth\AdminLoginController@Authenticate')->name('admin.login');
    Route::get('admin/logout', 'AdminController@logout')->name('admin.logout');
    Route::get('admin/home', 'AdminController@index')->name('admin.home');

    //THE USER DASH AND FUNCTIONS
    Route::get('/logout', 'User\UserController@logout');
    Route::get('user/home', 'User\UserController@dashboard')->name('user.home');
    Route::get('user/report', 'User\UserController@showReportCrime')->name('new.crime');
    Route::POST('user/report/crime', 'User\UserController@ReportCrime')->name('save.crime');
    Route::get('user/report','User\UserController@getData')->name('get.county.data');
    Route::get('user/report/{id}','User\UserController@getPoliceStations')->name('get.police.stations');
    Route::get('/user/view/cases','User\UserController@showCases')->name('view.cases');
    

    
    Route::group(['middleware' => ['admin']], function () {
    
    //Investigating Officer
    Route::get('investigatingofficer', 'InvestigatingOfficerController@index')->name('index.io');
    Route::get('investigatingofficer/new', 'InvestigatingOfficerController@create')->name('new.io');
    Route::get('investigatingofficer/new','InvestigatingOfficerController@getData')->name('get.police.station.data');
    Route::post('investigatingofficer/save', 'InvestigatingOfficerController@store')->name('save.io');
    Route::get('investigatingofficer/edit/{id}', 'InvestigatingOfficerController@edit')->name('edit.io');
    Route::put('investigatingofficer/update/{id}', 'InvestigatingOfficerController@update')->name('update.io');
    Route::get('investigatingofficer/update/deactivate/{id}', 'InvestigatingOfficerController@deactivate')->name('deactivate.io');
    Route::get('investigatingofficer/update/activate/{id}', 'InvestigatingOfficerController@activate')->name('activate.io');


    //OCS
    Route::get('ocs', 'OCSController@index')->name('index.ocs');
    Route::get('ocs/new', 'OCSController@create')->name('new.ocs');
    Route::get('ocs/new','OCSController@getData')->name('get.police.station.data');
    Route::post('ocs/save', 'OCSController@store')->name('save.ocs');
    Route::get('ocs/edit/{id}', 'OCSController@edit')->name('edit.ocs');
    Route::put('ocs/update/{id}', 'OCSController@update')->name('update.ocs');
    Route::get('ocs/update/deactivate/{id}', 'OCSController@deactivate')->name('deactivate.ocs');
    Route::get('ocs/update/activate/{id}', 'OCSController@activate')->name('activate.ocs');

    //JO 
    Route::get('judicialofficer', 'JudicialOfficerController@index')->name('index.judicialofficer');
    Route::get('judicialofficer/new', 'JudicialOfficerController@create')->name('new.judicialofficer');
    Route::post('judicialofficer/save', 'JudicialOfficerController@store')->name('save.judicialofficer');
    Route::get('judicialofficer/edit/{id}', 'JudicialOfficerController@edit')->name('edit.judicialofficer');
    Route::put('judicialofficer/update/{id}', 'JudicialOfficerController@update')->name('update.judicialofficer');
    Route::get('judicialofficers/update/deactivate/{id}', 'JudicialOfficerController@deactivate')->name('deactivate.judicialofficer');
    Route::get('judicialofficers/update/activate/{id}', 'JudicialOfficerController@activate')->name('activate.judicialofficer');

    //PROSECUTOR
    Route::get('prosecutor', 'ProsecutorController@index')->name('index.prosecutor');
    Route::get('prosecutor/new', 'ProsecutorController@create')->name('new.prosecutor');
    Route::post('prosecutor/save', 'ProsecutorController@store')->name('save.prosecutor');
    Route::get('prosecutor/edit/{id}', 'ProsecutorController@edit')->name('edit.prosecutor');
    Route::put('prosecutor/update/{id}', 'ProsecutorController@update')->name('update.prosecutor');
    Route::get('prosecutor/update/deactivate/{id}', 'ProsecutorController@deactivate')->name('deactivate.prosecutor');
    Route::get('prosecutor/update/activate/{id}', 'ProsecutorController@activate')->name('activate.prosecutor');
    
    //USER 
    Route::get('user', 'UserController@index')->name('index.user');
    Route::get('user/update/deactivate/{id}', 'UserController@deactivate')->name('deactivate.prosecutor');
    Route::get('user/update/activate/{id}', 'UserController@activate')->name('activate.prosecutor');
    
    //CRIME 
    Route::get('/crime','CrimeController@ShowCrime')->name('show.crime');
    Route::get('/crime/location/chart', 'LocationChartController@getCounties')->name('charts');

    //CHARTS
    Route::get('/crime/chart', 'ChartController@generateChart')->name('charts');
    Route::get('/crime/offender/chart', 'SexChartController@generateSexChart')->name('charts');
    Route::get('/crime/location/chart', 'LocationChartController@generateLocationChart')->name('charts');
    

    //CASES
    Route::get('/case','CaseFileController@showCases')->name('show.cases');

});

//JO CREDENTIALS AND HOME
    Route::group(['middleware' => ['judicialofficer_guest']], function () {
    Route::get('judicialofficer/login','Judicialofficer\JudicialOfficerLoginController@LoginForm')->name('jo.login.form');
Route::post('judicialofficer/login','Judicialofficer\JudicialOfficerLoginController@Authenticate')->name('jo.login');
});
Route::group(['middleware' => ['judicialofficer_auth']], function () {
    Route::get('judicialofficer/logout','Judicialofficer\JudicialOfficerController@logout')->name('jo.logout');
    Route::get('judicialofficer/home','Judicialofficer\JudicialOfficerController@index')->name('jo.home');
    Route::get('judicialofficer/view/cases','Judicialofficer\JudicialOfficerController@viewCases')->name('jo.view.cases');
    Route::get('judicialofficer/view/case/{id}','Judicialofficer\JudicialOfficerController@viewCase')->name('jo.view.case');
    Route::get('judicialofficer/update/case/{id}','Judicialofficer\JudicialOfficerController@updateCase')->name('jo.update.case');
    Route::get('judicialofficer/offender/{id}','Judicialofficer\JudicialOfficerController@offenderForm')->name('jo.offender.form');
    Route::post('judicialofficer/add/offender/{id}','Judicialofficer\JudicialOfficerController@addOffender')->name('jo.add.offender');
});


//PROSECUTOR CREDENTIALS AND HOME
Route::group(['middleware' => ['prosecutor_guest']], function () {
    Route::get('prosecutor/login','Prosecutor\ProsecutorLoginController@LoginForm')->name('prosecutor.login.form');
    Route::post('prosecutor/login','Prosecutor\ProsecutorLoginController@Authenticate')->name('prosecutor.login');
});
Route::group(['middleware' => ['prosecutor_auth']], function () {
    Route::get('prosecutor/home','Prosecutor\ProsecutorController@index')->name('prosecutor.home');
    Route::get('prosecutor/assigned/cases','Prosecutor\ProsecutorController@viewCases')->name('prosecutor.view.casses');
    Route::get('prosecutor/view/case/{id}','Prosecutor\ProsecutorController@viewCase')->name('prosecutor.view.cases');
    Route::get('prosecutor/assign/jo/{id}','Prosecutor\ProsecutorController@assignJo')->name('prosecutor.assign.jo');
    Route::put('prosecutor/update/jo/{id}','Prosecutor\ProsecutorController@updateJo')->name('prosecutor.update.jo');
    Route::get('prosecutor/logout','Prosecutor\ProsecutorController@logout')->name('prosecutor.logout');
});

//OCS CREDENTIALS AND HOME
Route::group(['middleware' => ['OCS_guest']], function () {
    Route::get('ocs/login','OCS\OCSLoginController@LoginForm')->name('ocs.login.form');
    Route::post('ocs/login','OCS\OCSLoginController@Authenticate')->name('ocs.login');
});
Route::group(['middleware' => ['OCS_auth']], function () {
    Route::get('ocs/home','OCS\OCSController@index')->name('ocss.home');
    Route::get('ocs/logout','OCS\OCSController@logout')->name('ocs.logout');
    Route::get('ocs/reported/crimes','OCS\OCSController@viewCrimes')->name('ocs.view.crimes');
    Route::get('ocs/reported/crimes/assign/{id}', 'OCS\OCSController@assignCrime')->name('assign.crime');
    Route::put('ocs/crime/update/{id}', 'OCS\OCSController@updateCrime')->name('update.crime');
    Route::get('ocs/cases/{id}','OCS\OCSController@viewCase')->name('ocs.view.cases');
    Route::get('ocs/assign/prosecutor/{id}','OCS\OCSController@assignProsecutor')->name('ocs.assign.prosecutor');
    Route::put('ocs/update/prosecutor/{id}','OCS\OCSController@updateProsecutor')->name('ocs.assign.prosecutor');
    Route::get('ocs/close/case/{id}','OCS\OCSController@closeFile')->name('ocs.close.file');
    Route::get('ocs/view/investigating/officers', 'OCS\OCSController@showIO')->name('show.io');
       
});



//INVESTIGATING OFFICER CREDENTIALS AND HOME

Route::group(['middleware' => ['investigating_officer_guest']], function () {
    Route::get('investigatingofficer/login','InvestigatingOfficer\InvestigatingOfficerLoginController@LoginForm')->name('io.login.form');
    Route::post('investigatingofficer/login','InvestigatingOfficer\InvestigatingOfficerLoginController@Authenticate')->name('io.login');
});
Route::group(['middleware' => ['investigating_officer_auth']], function () {
    Route::get('investigatingofficer/home','InvestigatingOfficer\InvestigatingOfficerController@index')->name('io.home');
    // Route::get('investigatingofficer/home','InvestigatingOfficer\InvestigatingOfficerController@countSubmitted')->name('count.submitted');
    Route::get('investigatingofficer/logout','InvestigatingOfficer\InvestigatingOfficerController@logout')->name('io.logout');
    Route::get('investigatingofficer/assigned/crimes','InvestigatingOfficer\InvestigatingOfficerController@viewCrimes')->name('io.view.crimes');
    Route::get('investigatingofficer/create/case/{id}','InvestigatingOfficer\InvestigatingOfficerController@createCase')->name('io.create.case');
    Route::put('investigatingofficer/submit/case/{id}','InvestigatingOfficer\InvestigatingOfficerController@submitCase')->name('io.submit.case');
});